import styles from './app.module.scss';
import { Board } from "./game/board";

export function App() {
  return (
    <main>
      <section>
        <h1 className={styles['title']}>2048</h1>

        <Board />

        <p>
          <strong>HOW TO PLAY:</strong> Use your <strong>arrow keys</strong> to move the tiles.
          Tiles with the same number <strong>merge into one</strong> when they touch.
          Add them up to reach <strong>2048!</strong>
        </p>
      </section>
    </main>
  );
}

export default App;
